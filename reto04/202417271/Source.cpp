#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int main() {
    srand(time(0));

    int tamano = rand() % 401 + 100;

    int arreglo[tamano];

    for (int i = 0; i < tamano; ++i) {
        arreglo[i] = rand() % 10000 + 1;
    }

    cout << "Arreglo de tamano " << tamano << " con numeros aleatorios entre 1 y 10000:\n";
    for (int i = 0; i < tamano; ++i) {
        cout << arreglo[i] << " ";
    }

    return 0;
}